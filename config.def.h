/* See LICENSE file for copyright and license details. */

/* Used for multimedia keys */
#include <X11/X.h>
#include <X11/XF86keysym.h>
#include <X11/Xlib.h>

/* appearance */
static const unsigned int borderpx  = 4;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 3;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray        = 1;        /* 0 means no systray */
static const unsigned int gappih    = 7;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 7;       /* vert inner gap between windows */
static const unsigned int gappoh    = 7;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 7;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const double activeopacity   = 1.0f;     /* Window opacity when it's focused (0 <= opacity <= 1) */
static const double inactiveopacity = 0.875f;   /* Window opacity when it's inactive (0 <= opacity <= 1) */
static       Bool bUseOpacity       = True;     /* Starts with opacity on any unfocused windows */
static const char *fonts[]          = { "FiraCode Nerd Font:size=12:antialias=true:autohint=true", "Font Awesome 5 Free:size=12:autohint=true" };
static const char dmenufont[]       = "monospace:size=12";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#1b4d3e";
static const unsigned int baralpha = 0xc0;
static const unsigned int borderalpha = OPAQUE;

static const char *colors[][3]      = {
	/*                     fg         bg         border   */
	[SchemeNorm]     = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]      = { col_gray4, col_cyan,  col_cyan  },
	[SchemeStatus]   = { col_gray3, col_gray1, "#000000"  }, // Statusbar right {text,background,not used but cannot be empty}
	[SchemeTagsSel]  = { col_gray4, col_cyan,  "#000000"  }, // Tagbar left selected {text,background,not used but cannot be empty}
	[SchemeTagsNorm] = { col_gray3, col_gray1, "#000000"  }, // Tagbar left unselected {text,background,not used but cannot be empty}
	[SchemeInfoSel]  = { col_gray4, col_cyan,  "#000000"  }, // infobar middle  selected {text,background,not used but cannot be empty}
	[SchemeInfoNorm] = { col_gray3, col_gray1, "#000000"  }, // infobar middle  unselected {text,background,not used but cannot be empty}
};

static const unsigned int alphas[][3]      = {
	/*                   fg      bg        border     */
	[SchemeNorm]     = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]      = { OPAQUE, baralpha, borderalpha },
	[SchemeStatus]   = { OPAQUE, baralpha, borderalpha },
	[SchemeTagsSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeTagsNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeInfoSel]  = { OPAQUE, baralpha, borderalpha },
	[SchemeInfoNorm] = { OPAQUE, baralpha, borderalpha },
};

static const char *const autostart[] = {
	"lxsession", NULL,
	"/usr/bin/gnome-keyring-daemon", "--start", "--components=pkcs11,secrets,ssh", NULL,
	"picom", NULL,
	"nitrogen", "--restore", NULL,
	"/usr/bin/dunst", NULL,
	"nm-applet", NULL,
	"dropbox", NULL,
	"udiskie", "--tray", NULL,
	"/usr/bin/sxhkd", NULL,
	//"sh", "-c", "~/.Scripts/HuionConfig.sh", NULL,
	// "sh", "-c", "~/.config/polybar/launch.sh", NULL,
	// "", NULL,
	NULL /* terminate */
};

/* tagging */
static const char *tags[] = { "", "", "", "", "", "" };
// The font use for the numbers is the Nerd Fonts (nf-mdi-numeric)
static const char *tagsalt[] = { "", "", "", "", "", "" };

/* default layout per tags */
/* The first element is for all-tag view, following i-th element corresponds to */
/* tags[i]. Layout is referred using the layouts array index.*/
static int def_layouts[1 + LENGTH(tags)]  = { 0, 1, 0, 2, 1, 3, 3};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class             instance       title       tags mask     isfloating   monitor */
	/* 1) WWW */
	{ "Chromium",        NULL,          NULL,        1,               0,         -1, 0 },
	{ "Brave-browser",   NULL,          NULL,        1,               0,         -1, 0 },
	{ "firefox",         NULL,          NULL,        1,               0,         -1, 0 },
	{ "Google-chrome",   NULL,          NULL,        1,               0,         -1, 0 },
	{ "Vivaldi-stable",  NULL,          NULL,        1,               0,         -1, 0 },

	/* 2) Code */
	{ "Atom",            NULL,          NULL,        1 << 1,          0,         -1, 0 },
	{ "code-oss",        NULL,          NULL,        1 << 1,          0,         -1, 0 },
	{ "Emacs",           NULL,          NULL,        1 << 1,          0,         -1, 0 },
	{ "jetbrains-clion", NULL,          NULL,        1 << 1,          0,         -1, 1 },
	{ "UE4Editor",       NULL,          NULL,        1 << 1,          0,         -1, 1 },

	/* 3) Terminal */
	//{ "Alacritty",       NULL,          NULL,        1 << 2,          0,         -1 },
	//{ "kitty",           NULL,          NULL,        1 << 2,          0,         -1 },

	/* 4) GFX */
	{ "Blender",         NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "Gimp",            NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "krita",           NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "Steam",           NULL,          NULL,        1 << 3,          0,         -1, 0 },
	{ "TexturePacker",   NULL,          NULL,        1 << 3,          0,         -1, 0 },

	/* 5) Music */
	{ "Cadence",         NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "elisa",           NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Guitarix",        NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Spotify",         NULL,          NULL,        1 << 4,          0,         -1, 0 },
	{ "Stremio",         NULL,          NULL,        1 << 4,          0,         -1, 0 },

	/* 6) Chat */
	{ "discord",            NULL,          NULL,        1 << 5,          0,         -1, 0 },
	{ NULL,                 NULL,  "FacebookMessenger", 1 << 5,          0,         -1, 0 },
	{ "Slack",              NULL,          NULL,        1 << 5,          0,         -1, 0 },
	{ "Whatsapp-for-linux", NULL,          NULL,        1 << 5,          0,         -1, 0 },
};

/* layout(s) */
static const float mfact     = 0.65; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                          KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,              KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,                KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask,    KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "rofi", "-show", "combi" };
static const char *termcmd[]  = { "alacritty", NULL };

static const char *printscrcmd[] = { "spectacle", NULL };
static const char *browsercmd[][3]  = { { "brave", NULL },
                                        { "vivaldi-stable", NULL },
                                        { "vivaldi-stable", "--incognito", NULL },
                                        { "chromium", NULL },
                                        { "firefox", NULL } };
static const char *lockcmd[] = { "i3lock-fancy-multimonitor", "--blur=5x4", NULL };

static Key keys[] = {
	/* modifier             key            function        argument */
/*
	{ MODKEY,               XK_p,          spawn,          {.v = dmenucmd } },
	// Applications
	{ MODKEY|ShiftMask,     XK_Return,     spawn,          {.v = termcmd } },
	{ 0,                    XK_Print,      spawn,          {.v = printscrcmd } },
	// Browsers (use B as chord)
	// { MODKEY,               XK_1,          spawn,          {.v = browsercmd[0] } },
	// { MODKEY,               XK_2,          spawn,          {.v = browsercmd[1] } },
	// { MODKEY,               XK_3,          spawn,          {.v = browsercmd[2] } },
	// { MODKEY,               XK_4,          spawn,          {.v = browsercmd[3] } },
	// { MODKEY,               XK_5,          spawn,          {.v = browsercmd[4] } },
	// Update the update package status
	{ MODKEY|ShiftMask,     XK_u,          spawn,          SHCMD("kill -38 $(pidof dwmblocks)") },
	// Backlight (Up/Down)
	{ 0,                    0x1008FF02,    spawn,          SHCMD("light -A 2") },
	{ 0,                    0x1008FF03,    spawn,          SHCMD("light -U 2") },
	// Volume (Up/Down/Mute/Mute Input)
	{ 0,                    0x1008FF13,    spawn,          SHCMD("pulseaudio-ctl up 2; kill -37 $(pidof dwmblocks)") },
	{ 0,                    0x1008FF11,    spawn,          SHCMD("pulseaudio-ctl down 2; kill -37 $(pidof dwmblocks)") },
	{ 0,                    0x1008FF12,    spawn,          SHCMD("pulseaudio-ctl mute; kill -37 $(pidof dwmblocks)") },
	{ MODKEY,               0x1008FF12,    spawn,          SHCMD("pulseaudio-ctl mute-input; kill -37 $(pidof dwmblocks)") },
	// Sleep/LockScreen
	{ 0,                    0x1008FF2F,    spawn,          {.v = lockcmd } },
	// Navigate stack
	{ MODKEY,               XK_Down,       focusstack,     {.i = +1 } },
	{ MODKEY,               XK_Up,         focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,     XK_Down,       movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_Up,         movestack,      {.i = -1 } },
	{ MODKEY,               XK_Tab,        rotatestack,    {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_Tab,        rotatestack,    {.i = -1 } },
	{ MODKEY,               XK_space,      focusmaster,    {0} },
	// Set on master
	{ MODKEY,               XK_Return,     zoom,           {0} },
	{ MODKEY|Mod1Mask,      XK_0,          togglegaps,     {0} },
	// { MODKEY|Mod1Mask|ShiftMask, XK_0,          defaultgaps,    {0} },
	// Increase/Decrease Clients on master
	{ MODKEY,               XK_i,          incnmaster,     {.i = +1 } },
	{ MODKEY,               XK_d,          incnmaster,     {.i = -1 } },
	// Increase/Decrease Master/Columns Size
	{ MODKEY,               XK_Left,       setmfact,       {.f = -0.05} },
	{ MODKEY,               XK_Right,      setmfact,       {.f = +0.05} },
	{ MODKEY|Mod1Mask,      XK_Up,         setcfact,       {.f = +0.25} },
	{ MODKEY|Mod1Mask,      XK_Down,       setcfact,       {.f = -0.25} },
	{ MODKEY|Mod1Mask,      XK_o,          setcfact,       {.f =  0.00} },
	// Switch to last tag
	// { MODKEY,               -1,         XK_Tab,        view,           {0} },
	// Kill client
	{ MODKEY|ShiftMask,     XK_c,          killclient,     {0} },
	// Layouts (use L as chord key)
	// { MODKEY,               XK_q,          setlayout,      {.v = &layouts[0]} },
	// { MODKEY,               XK_w,          setlayout,      {.v = &layouts[1]} },
	// { MODKEY,               XK_e,          setlayout,      {.v = &layouts[2]} },
	// { MODKEY,               XK_r,          setlayout,      {.v = &layouts[3]} },
	// { MODKEY,               XK_t,          setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ControlMask,   XK_space,      setlayout,      {0} },
	{ MODKEY|ShiftMask,     XK_space,      togglefloating, {0} },
	// VIEW & TAG EVERYTHING
	// { MODKEY,               XK_0,          view,           {.ui = ~0 } },
	// { MODKEY|ShiftMask,     XK_0,          tag,            {.ui = ~0 } },
	// Monitor navigation
	{ MODKEY,               XK_q,          focusmon,       {.i = -1 } },
	{ MODKEY,               XK_e,          focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,     XK_q,          tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,     XK_e,          tagmon,         {.i = +1 } },
	// See Alternative name (number of the tag)
	{ MODKEY,               XK_n,          togglealttag,   {0} },
	{ MODKEY|ShiftMask,     XK_n,          togglebar,      {0} },
	//Tag navigation
	TAGKEYS(                XK_1,                          0)
	TAGKEYS(                XK_2,                          1)
	TAGKEYS(                XK_3,                          2)
	TAGKEYS(                XK_4,                          3)
	TAGKEYS(                XK_5,                          4)
	TAGKEYS(                XK_6,                          5)
	TAGKEYS(                XK_7,                          6)
	TAGKEYS(                XK_8,                          7)
	TAGKEYS(                XK_9,                          8)
	{ MODKEY|ShiftMask,     XK_y,          quit,           {0} },
*/
    { MODKEY|ShiftMask,     XK_y,          quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "focusstack",     focusstack },
	{ "movestack",      movestack },
	{ "rotatestack",    rotatestack },
	{ "setmfact",       setmfact },
	{ "setcfact",       setcfact },
	{ "togglealttag",   togglealttag },
	{ "togglebar",      togglebar },
	{ "toggleopacity",  toggleopacity },
	{ "incnmaster",     incnmaster },
	{ "togglefloating", togglefloating },
	{ "focusmaster",    focusmaster },
	{ "focusmon",       focusmon },
	{ "tagmon",         tagmon },
	{ "zoom",           zoom },
	{ "view",           view },
	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "togglegaps",     togglegaps },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "killclient",     killclient },
	{ "quit",           quit },
	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
};
